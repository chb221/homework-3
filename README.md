
----- HW3 -----

Question1

----- String in Rust -----

string = '"', {all_characters - '"' - '\' | white_space | "\"" | "\\"}, '"';
white_space = ? white_space characters ? ;
all_characters = ? all visible characters ? ;

----- Comment in Rust -----

comment = line_comment | block_comment;
line_comment = "//", {all_characters |
 white_space - '\n'};
block_comment = "/*", {block_comment | no_comment_character}, "*/";
no_comment_character = {all_characters - '*' - '/' | '/', all_characters - '*' | '*', all_characters - '/' | white_space}
all_characters = ? all visible characters ? ;
white_space = ? white_space characters ? ;

----- Email Address -----

email = name, '@', company, '.', organization_code, ['.', country_code];
name = {letter | digit};
company = {letter}
organization_code = "arpa" | "com" | "edu" | "gov" | "int" | "mil" | "nato" | "net" | "org";
country_code = letter, letter;
letter = 'a'-'z' | 'A'-'Z';
digit = '0'-'9';

----- Phone Number -----

phone number = NPA, '-', NXX, '-', XXXX;
NPA = digit, digit, digit;
NXX = digit - '0' - '1', digit, digit;
XXXX = digit, digit, digit, digit;
digit = '0'-'9';

----- Numeric Constant -----

numeric = binary | octal | decimal | hexadecimal | decimal_constant;
binary = '0', 'b', {'0' | '1'};
octal = '0', 'o', {'0'-'9'};
decimal = ['-'], (natural, ['.', {'0'}, natural] | '0', '.', {'0'}, natural);
hexadecimal = '0', 'x', {'0'-'9' | 'A'-'F'};
decimal_constant = decimal, 'e', ('0' | ['-'], natural);
natural = digit - '0', {digit | {'_'}, digit};
digit = '0'-'9';

-------------------------------------------------------------------------

Question2

1. A lexical error, detected by the scanner.

let number+: u32 = 24; // '+' is an improper character.

2. A syntax error, detected by the parser.

let number: u32 = 24 // missing semicolon

3. A static semantic error, detected by semantic analysis.

let number: u32 = 24.1; // 24.1 is a floating point.

4. A ownership error, detected by the borrow checker.

let number: u32 = 24; 
func(number); // lose ownership here
print!({},number); // cannot use number again

5. A lifetime error, detected through lifetime analysis of variables.

let number: u32 = 24;
if(number > 1){
	let new_number: u32 = 10;
	number = &new_number
}
print!({}, number); 

-------------------------------------------------------------------------

Question3


The limit of usize is 32 bits or 64 bits depending on the machine such as the limit is 18446744073709551615 on 64 bits machine.
During compiling, Rust will not allow to overflow.
The programs will run on both machines/compilers.

-------------------------------------------------------------------------

Question4

It's difficult because we cannot perfectly debug the code especially when there are many lines of code. 

In my opinion, writing a unit test for each function helps a lot.

We can catch bugs of function by comparing the result of function with expected result. Also, some obvious bugs such as out of boundary and ownership will be debugged by compiling. 

The common invisible bug is runtime error such as looping forever.

-------------------------------------------------------------------------

Question5

1. Construct a parse tree for the input string "print(x)".

statement
| subroutine_call
  | identifier
    | p
    | r
    | i
    | n
    | t
  | "("
  | argument_list
    | expression
      | primary
        | identifier
          | x
      | expression_tail
        | e
    | argument_tail
      | e
  | ")" 

2. Construct a parse tree for the input string "y := add(a,b)".

statement
| assignment
  | identifier
    | y
  | ":="
  | expression
    | primary
      | subroutine_call
        | identifier
          | a
          | d
          | d
        | "("
        | argument_list
          | expression
            | primary
              | identifier
                | a
            | expression_tail
              | e
          | argument_tail
            | ","
            | argument_list
              | expression
                | primary
                  | identifier
                    | b
                | expression_tail
                  | e
              | argument_tail
                | e
        | ")"
    | expression_tail
      | e

3. Construct a parse tree for the input string "z := 1 + 2 * 3".

statement
| assignment 
  | identifier 
    | z
  | ":="
  | expression
    | primary
      | identifier
        | 1
    | expression_tail
      | operator 
        | +
      | expression
        | primary
          | identifier
            | 2
        | expression_tail
          | operator
            | *
          | expression
            | primary
              | identifier
                | 3
            | expression_tail
              | e

4. Construct a parse tree for the input string "x := 9 * sin(x)".

statement
| assignment
  | identifier 
    | x
  | ":="
  | expression
    | primary
      | identifier
        | 9
    | expression_tail
      | operator
        | *
      | expression
        | primary
          | subroutine_call
            | identifier
              | s
              | i
              | n
            | "("
            | argument_list
              | expression
                | primary
                  | identifier
                    | x
                | expression_tail
                  | e
              | argument_tail
                | e
            | ")"
        | expression_tail
          | e
  
-------------------------------------------------------------------------

Question6
 
1. Describe in English the language generated by this grammar. Hint: B stands for "balanced", N stands for "non-balanced".

G is the a sequence of "(", ")", and "]" representing balanced and non-balanced strings generated from "(" and ")". A group of (...] represents non-balanced strings and (...) represents balanced string. 

2. Give a parse tree for the string "((]()".

G
| G
  | G
    | e
  | N
    | "("
    | L
      | L
        | e
      | "(" 
    | "]"
| B
  | "("
  | E
    | e 
  | ")"


3. Give a parse tree for the string "((()))".

G
| G
  | e
| B
  | "("
  | E
    | E
      | e
    | "("
    | E
      | E
        | e
      | "("
      | E
        | e
      | ")"
    | ")" 
  | ")"

